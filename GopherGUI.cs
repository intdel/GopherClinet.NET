﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;

namespace GopherClient
{
    public partial class GopherGUI : Form
    {
        String ipAddress;
        String vpnProcessOutput = "";
        bool connected = false;
        BackgroundWorker connectVPNWorker = null;
        Object mutex = new object();
        public GopherGUI()
        {
            InitializeComponent();
            CenterToScreen();
            this.Text = GopherProperties.version;

            //Add eventhandlers
            vpnGetIPButton.Click += new EventHandler(GetIP_click);
            vpnConnectButton.Click += new EventHandler(ConnectVPN_Click);
            vpnDisonnectButton.Click += new EventHandler(DisconnectVPN_Click);
        }

        static void Main()
        {
            Application.EnableVisualStyles();
            Application.Run(new GopherGUI());
        }

        private void DisconnectVPN_Click(object sender, System.EventArgs e)
        {
            WaitStart("Trying to cancel...");
            if (connected && connectVPNWorker != null)
            {
                connectVPNWorker.CancelAsync();
            }
        }

        private void ConnectVPN_Click(object sender, System.EventArgs e)
        {
            connectVPNWorker = new BackgroundWorker();
            connectVPNWorker.WorkerReportsProgress = true;
            connectVPNWorker.WorkerSupportsCancellation = true;
            connectVPNWorker.DoWork += new DoWorkEventHandler(ConnectVPN_Bw);
            connectVPNWorker.ProgressChanged += new ProgressChangedEventHandler(ConnectVPN_Bw_Progress);
            connectVPNWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(ConnectVPN_Bw_Done);

            WaitStart("Connecting...");
            connectVPNWorker.RunWorkerAsync();
        }

        private void GetIP_click(object sender, System.EventArgs e)
        {
            BackgroundWorker getIP_bw = new BackgroundWorker();
            getIP_bw.DoWork += new DoWorkEventHandler(GetIP_bw);
            getIP_bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(GetIP_bw_done);

            WaitStart("Getting IP");
            getIP_bw.RunWorkerAsync();
        }

        private void ConnectVPN_Bw(object sender, System.EventArgs e)
        {
            Process vpnProcess = new Process();
            BackgroundWorker bw = sender as BackgroundWorker;
            //vpnProcess.StartInfo.FileName = "C:\\Windows\\System32\\cmd.exe";
            //vpnProcess.StartInfo.Arguments = "/C ping -n 180 localhost";
            vpnProcess.StartInfo.FileName = @"..\bin\openvpn.exe";
            vpnProcess.StartInfo.Arguments = "openvpn.ovpn";
            //vpnProcess.StartInfo.Arguments = "/C echo Works";
            vpnProcess.StartInfo.RedirectStandardOutput = true;
            vpnProcess.StartInfo.UseShellExecute = false;
            GopherCLI gopher = new GopherCLI();
            String ipAddress = gopher.GetIP();

            //WARNING DEBUG
            //Needs to be changed so that if ipAddress is not -1, it connects
            if (ipAddress == "-1" || ipAddress != "-1")
            {
                String input = "";
                bool cancelled = false;
                this.ipAddress = ipAddress;

                vpnProcessOutput = "Generating new config...";
                bw.ReportProgress(1);
                //Get new config
                gopher.GenerateOVPNOutputFile();
                vpnProcessOutput = "Generating new config... Done";
                bw.ReportProgress(1);

                vpnProcess.Start();
                while ((input = vpnProcess.StandardOutput.ReadLine()) != null && !cancelled)
                {
                    lock (mutex)
                    {
                        vpnProcessOutput = input + "\n";

                        bw.ReportProgress(1);
                    }

                    if (bw.CancellationPending)
                    {
                        lock (mutex)
                        {
                            vpnProcessOutput = "Cancelling now...\n";
                        }
                        bw.ReportProgress(1);
                        vpnProcess.CloseMainWindow();
                        vpnProcess.WaitForExit();
                        cancelled = true;
                    }
                }
            } else
            {
                lock (mutex)
                {
                    vpnProcessOutput = "IPAddress returned as " + ipAddress + ". So did not connect.";
                }
                bw.ReportProgress(1);
            }

        }

        private void ConnectVPN_Bw_Done(object sender, System.EventArgs e)
        {
            vpnCurrentIPLabel.Visible = false;
            connected = false;
            vpnConnectionLabel.Text = "VPN Disconnected";
            vpnConnectionLabel.ForeColor = Color.Red;
            WaitDone(); 
        }

        private void ConnectVPN_Bw_Progress(object sender, System.EventArgs e)
        {
            lock (mutex)
            {
                OutputText(vpnProcessOutput, false);
            }
                //Change it back to "Initialization Sequence Completed"
            if (vpnProcessOutput.Contains("Initialization Sequence Completed") && !connected)
            {
                vpnCurrentIPLabel.Text = ipAddress;
                vpnCurrentIPLabel.Visible = true;
                connected = true;
                vpnConnectionLabel.Text = "VPN Connected";
                vpnConnectionLabel.ForeColor = Color.Green;
                WaitDone();
            }

            if (vpnProcessOutput.ToLower().Contains("error") || vpnProcessOutput.ToLower().Contains("fail"))
            {
                vpnConnectionLabel.Text = "VPN Status Unknown (Errors detected)";
                vpnConnectionLabel.ForeColor = Color.YellowGreen;
            }

        }

        private void GetIP_bw (object sender, System.EventArgs e)
        {
            GopherCLI cli = new GopherCLI();
            ipAddress = cli.GetIP();
        }

        private void GetIP_bw_done (object sender, System.EventArgs e)
        {
            OutputText("Read IP address is: " + ipAddress, true);
            WaitDone();
        }

        private void OutputText(String text, bool newline)
        {
            if (newline)
            {
                vpnOutputTextBox.Text += text + "\n";
            } else
            {
                vpnOutputTextBox.Text += text;
            }
        }

        private void WaitStart(string cause)
        {
            vpnStatusLabel.Text = "Doing: " + cause;
            vpnProgressBar.Visible = true;
            this.UseWaitCursor = true;
            vpnConnectButton.Enabled = false;
            vpnDisonnectButton.Enabled = false;
            vpnGetIPButton.Enabled = false;
        }

        private void WaitDone()
        {
            vpnProgressBar.Visible = false;
            vpnStatusLabel.Text = "Ready";
            this.UseWaitCursor = false;
            if (connected)
            {
                vpnDisonnectButton.Enabled = true;
            }
            else
            {
                vpnConnectButton.Enabled = true;
            }

            vpnGetIPButton.Enabled = true;
        }

      
    }
}
