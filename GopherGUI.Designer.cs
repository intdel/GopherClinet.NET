﻿namespace GopherClient
{
    partial class GopherGUI
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.vpnOutputBox = new System.Windows.Forms.GroupBox();
            this.vpnOutputTextBox = new System.Windows.Forms.RichTextBox();
            this.vpnControlBox = new System.Windows.Forms.GroupBox();
            this.vpnGetIPButton = new System.Windows.Forms.Button();
            this.vpnDisonnectButton = new System.Windows.Forms.Button();
            this.vpnConnectButton = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.vpnConnectionLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.vpnCurrentIPLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.vpnStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.vpnProgressBar = new System.Windows.Forms.ToolStripProgressBar();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.vpnOutputBox.SuspendLayout();
            this.vpnControlBox.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // vpnOutputBox
            // 
            this.vpnOutputBox.Controls.Add(this.vpnOutputTextBox);
            this.vpnOutputBox.Location = new System.Drawing.Point(16, 15);
            this.vpnOutputBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.vpnOutputBox.Name = "vpnOutputBox";
            this.vpnOutputBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.vpnOutputBox.Size = new System.Drawing.Size(760, 236);
            this.vpnOutputBox.TabIndex = 1;
            this.vpnOutputBox.TabStop = false;
            this.vpnOutputBox.Text = "VPN Output / Server result";
            // 
            // vpnOutputTextBox
            // 
            this.vpnOutputTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.vpnOutputTextBox.Location = new System.Drawing.Point(8, 23);
            this.vpnOutputTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.vpnOutputTextBox.Name = "vpnOutputTextBox";
            this.vpnOutputTextBox.ReadOnly = true;
            this.vpnOutputTextBox.Size = new System.Drawing.Size(743, 205);
            this.vpnOutputTextBox.TabIndex = 0;
            this.vpnOutputTextBox.Text = "";
            // 
            // vpnControlBox
            // 
            this.vpnControlBox.Controls.Add(this.vpnGetIPButton);
            this.vpnControlBox.Controls.Add(this.vpnDisonnectButton);
            this.vpnControlBox.Controls.Add(this.vpnConnectButton);
            this.vpnControlBox.Location = new System.Drawing.Point(24, 265);
            this.vpnControlBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.vpnControlBox.Name = "vpnControlBox";
            this.vpnControlBox.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.vpnControlBox.Size = new System.Drawing.Size(752, 84);
            this.vpnControlBox.TabIndex = 2;
            this.vpnControlBox.TabStop = false;
            this.vpnControlBox.Text = "VPN Control";
            // 
            // vpnGetIPButton
            // 
            this.vpnGetIPButton.Location = new System.Drawing.Point(547, 23);
            this.vpnGetIPButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.vpnGetIPButton.Name = "vpnGetIPButton";
            this.vpnGetIPButton.Size = new System.Drawing.Size(100, 28);
            this.vpnGetIPButton.TabIndex = 2;
            this.vpnGetIPButton.Text = "Get IP";
            this.vpnGetIPButton.UseVisualStyleBackColor = true;
            // 
            // vpnDisonnectButton
            // 
            this.vpnDisonnectButton.Enabled = false;
            this.vpnDisonnectButton.Location = new System.Drawing.Point(331, 23);
            this.vpnDisonnectButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.vpnDisonnectButton.Name = "vpnDisonnectButton";
            this.vpnDisonnectButton.Size = new System.Drawing.Size(100, 28);
            this.vpnDisonnectButton.TabIndex = 1;
            this.vpnDisonnectButton.Text = "Disconnect";
            this.vpnDisonnectButton.UseVisualStyleBackColor = true;
            // 
            // vpnConnectButton
            // 
            this.vpnConnectButton.Location = new System.Drawing.Point(131, 23);
            this.vpnConnectButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.vpnConnectButton.Name = "vpnConnectButton";
            this.vpnConnectButton.Size = new System.Drawing.Size(100, 28);
            this.vpnConnectButton.TabIndex = 0;
            this.vpnConnectButton.Text = "Connect";
            this.vpnConnectButton.UseVisualStyleBackColor = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.vpnConnectionLabel,
            this.vpnCurrentIPLabel,
            this.vpnStatusLabel,
            this.vpnProgressBar});
            this.statusStrip1.Location = new System.Drawing.Point(0, 377);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Padding = new System.Windows.Forms.Padding(1, 0, 19, 0);
            this.statusStrip1.Size = new System.Drawing.Size(792, 25);
            this.statusStrip1.TabIndex = 3;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // vpnConnectionLabel
            // 
            this.vpnConnectionLabel.ForeColor = System.Drawing.Color.Red;
            this.vpnConnectionLabel.Name = "vpnConnectionLabel";
            this.vpnConnectionLabel.Size = new System.Drawing.Size(131, 20);
            this.vpnConnectionLabel.Text = "VPN Disconnected";
            // 
            // vpnCurrentIPLabel
            // 
            this.vpnCurrentIPLabel.Name = "vpnCurrentIPLabel";
            this.vpnCurrentIPLabel.Size = new System.Drawing.Size(75, 21);
            this.vpnCurrentIPLabel.Text = "$currentIP";
            this.vpnCurrentIPLabel.Visible = false;
            // 
            // vpnStatusLabel
            // 
            this.vpnStatusLabel.Name = "vpnStatusLabel";
            this.vpnStatusLabel.Size = new System.Drawing.Size(50, 20);
            this.vpnStatusLabel.Text = "Ready";
            // 
            // vpnProgressBar
            // 
            this.vpnProgressBar.Name = "vpnProgressBar";
            this.vpnProgressBar.Size = new System.Drawing.Size(133, 20);
            this.vpnProgressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.vpnProgressBar.Visible = false;
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            // 
            // GopherGUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 402);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.vpnControlBox);
            this.Controls.Add(this.vpnOutputBox);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.Name = "GopherGUI";
            this.Text = "GopherGUIForm";
            this.vpnOutputBox.ResumeLayout(false);
            this.vpnControlBox.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox vpnOutputBox;
        private System.Windows.Forms.RichTextBox vpnOutputTextBox;
        private System.Windows.Forms.GroupBox vpnControlBox;
        private System.Windows.Forms.Button vpnGetIPButton;
        private System.Windows.Forms.Button vpnDisonnectButton;
        private System.Windows.Forms.Button vpnConnectButton;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel vpnConnectionLabel;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
        private System.Windows.Forms.ToolStripStatusLabel vpnCurrentIPLabel;
        private System.Windows.Forms.ToolStripStatusLabel vpnStatusLabel;
        private System.Windows.Forms.ToolStripProgressBar vpnProgressBar;
    }
}