﻿using System;
using System.Net;
using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GopherClient
{
    internal struct GopherProperties
    {
        internal static readonly String version = "1.1 ALPHA .NET CLIENT 11-2016";
    }
   
    class GopherCLI
    {
        static ConfigParams configs = new ConfigParams();
        struct ConfigParams
        {
            internal String serverurl;
            internal String accesstoken;
            internal String username;
            internal String templateFile;
            internal String targetFile;

            //Check if any of the configs were left unconfigured
            public bool IsConfigured()
            {
                return (configs.serverurl != null && configs.accesstoken != null && configs.templateFile != null && configs.targetFile != null && configs.username != null);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("GOpher Portal VPN - GOpher Portal " + GopherProperties.version + "\n");
            GopherCLI gopher = new GopherCLI();

            //Read values from config
            try
            {
                gopher.PrepareConfig();
            }
            catch (Exception)
            {
                throw;
            }

            Console.WriteLine("Using: " + configs.serverurl);

            //If first argument is given, then evaluate choice
            if (args.Length != 1)
            {
                gopher.PrintHelp();
            }
            else
            {
                gopher.EvaluateChoice(args[0]);
            }

            Console.WriteLine("\nDone. Press any key to continue...");
            Console.ReadKey();

        }

        //Reads config file and populates configs struct 
        void PrepareConfig()
        {
            if (!configs.IsConfigured())
            {
                StreamReader reader = null;
                //Try to open config file, exit if can't be opened
                try
                {
                    reader = new StreamReader("gophclient.conf");
                } catch (FileNotFoundException exception)
                {
                    Console.WriteLine("Could not open config file: " + exception.ToString());
                    System.Environment.Exit(1);
                }

                String line = null;
            
                //Reads from config file and sets values in the ConfigParams struct
                while ((line = reader.ReadLine()) != null){

                    switch (line.Split('=')[0])
                    {
                        case "serverurl":
                            configs.serverurl = line.Split('=')[1];
                            break;
                        case "accesstoken":
                            configs.accesstoken = line.Split('=')[1];
                            break;
                        case "OpenVPNTemplate":
                            configs.templateFile = line.Split('=')[1];
                            break;
                        case "OpenVPNTarget":
                            configs.targetFile = line.Split('=')[1];
                            break;
                        case "username":
                            configs.username = line.Split('=')[1];
                            break;
                        default:
                            break;
                    }
                }
                reader.Close();
            }

            //Check if any of the configs were left unconfigured
            if (configs.serverurl == null || configs.accesstoken == null || configs.templateFile == null || configs.targetFile == null || configs.username == null)
            {
                Console.WriteLine("Not all properties have been set. Check config file.");
                throw new Exception("Not all properties have been set. Check config file.");
            }
        }

        void PrintHelp()
        {
            Console.Write("     ip -- list IPv4\nopenvpn -- Prepare OpenVPN config file\n");
        }

        //Used by Main to evaluate progam argument
        void EvaluateChoice(String choice)
        {
            switch (choice)
            {
                case "ip":
                    PrintIP();
                    break;
                case "openvpn":
                    GenerateOVPNOutputFile();
                    break;
                default:
                    PrintHelp();
                    break;
            }
        }

        //Prepares a WebClient
        WebClient GetWebClient()
        {
            if (!configs.IsConfigured())
            {
                PrepareConfig();
            }
            WebClient webClient = new WebClient() {
                Credentials = new NetworkCredential(configs.username, configs.accesstoken),
        };

            return webClient;
        }

        //Retrieves IP address
        internal String GetIP()
        {
            byte[] data = null;
            String output = "";

            WebClient webClient = GetWebClient();

            //Read URL from config file in the future
            try
            {
                data = webClient.DownloadData(configs.serverurl + "/ip");
            }
            catch (WebException exception)
            {
                output = exception.Message;
            }

            if (data != null)
            {
                output = Encoding.Default.GetString(data);
            }

            return output;
        }

        //Prints out VPN IP
        void PrintIP()  {


            Console.WriteLine("Obtaining IP...");

            
            Console.WriteLine(GetIP());
        }

        internal void GenerateOVPNOutputFile()
        {
            String line;
            StreamReader templateFileReader = null;
            StreamWriter outputFileWriter = null;
            String ipAddress = GetIP();

            if (ipAddress.Equals("-1"))
            {
                Console.WriteLine("VPN Server probably does not exist. Won't generate file!");
            } else {

                Console.WriteLine("Read IP address is " + ipAddress + "\nGenerating output file " + configs.targetFile + " from " + configs.templateFile);
                try
                {
                    templateFileReader = new StreamReader(configs.templateFile);
                    outputFileWriter = new StreamWriter(configs.targetFile);
                } catch (FileNotFoundException exception)
                {
                    Console.WriteLine("Could not open template file: " + exception.ToString());
                }

                while ((line = templateFileReader.ReadLine()) != null)
                {
                    line = line.Replace("$IP$", ipAddress);
                    outputFileWriter.WriteLine(line);
                }

                templateFileReader.Close();
                outputFileWriter.Close();
            }
            
        }
    }
}
