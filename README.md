# Go meets .NET

This is the .NET version **(C#)** of gopherclient for GopherPortal. It only supports printing current IP address and generating a
n OpenVPN Config file from a template.

gopherclient.conf is to be used from the Go gopherclient version.

Files needed in same directory:

- gopherclient.conf
- template file
- an output (target) file
